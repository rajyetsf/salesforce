@isTest
public class DatasetUploadResponseTest {
    
    // This test method should give 100% coverage
	static testMethod void testParse() {
		String json = '{'+
		'  \"id\": 1004804,'+
		'  \"name\": \"case_routing_intent.csv\",'+
		'  \"createdAt\": \"2017-06-22T19:31:58.000+0000.\",'+
		'  \"updatedAt\": \"2017-06-22T19:31:58.000+0000\",'+
		'  \"labelSummary\": {'+
		'    \"labels\": []'+
		'  },'+
		'  \"totalExamples\": 0,'+
		'  \"available\": false,'+
		'  \"statusMsg\": \"UPLOADING\",'+
		'  \"type\": \"text-intent\",'+
		'  \"object\": \"dataset\"'+
		'}';
		DatasetUploadResponse r = DatasetUploadResponse.parse(json);
		System.assert(r != null);

		json = '{\"TestAMissingObject\": { \"TestAMissingArray\": [ { \"TestAMissingProperty\": \"Some Value\" } ] } }';
		DatasetUploadResponse.LabelSummary objLabelSummary = new DatasetUploadResponse.LabelSummary(System.JSON.createParser(json));
		System.assert(objLabelSummary != null);
		System.assert(objLabelSummary.labels == null);

		json = '{\"TestAMissingObject\": { \"TestAMissingArray\": [ { \"TestAMissingProperty\": \"Some Value\" } ] } }';
		DatasetUploadResponse objDatasetUploadResponse = new DatasetUploadResponse(System.JSON.createParser(json));
		System.assert(objDatasetUploadResponse != null);
		System.assert(objDatasetUploadResponse.id == null);
		System.assert(objDatasetUploadResponse.name == null);
		System.assert(objDatasetUploadResponse.createdAt == null);
		System.assert(objDatasetUploadResponse.updatedAt == null);
		System.assert(objDatasetUploadResponse.labelSummary == null);
		System.assert(objDatasetUploadResponse.totalExamples == null);
		System.assert(objDatasetUploadResponse.available == null);
		System.assert(objDatasetUploadResponse.statusMsg == null);
		System.assert(objDatasetUploadResponse.type_Z == null);
		System.assert(objDatasetUploadResponse.object_Z == null);

		json = '{\"TestAMissingObject\": { \"TestAMissingArray\": [ { \"TestAMissingProperty\": \"Some Value\" } ] } }';
		DatasetUploadResponse.Labels objLabels = new DatasetUploadResponse.Labels(System.JSON.createParser(json));
		System.assert(objLabels != null);
        
        DatasetUploadResponse contrler = new DatasetUploadResponse();
	}

}