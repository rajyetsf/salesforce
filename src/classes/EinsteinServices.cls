/**
* This class is created to expose
* various Einstein Services as web services
* so that the same can be invoked from
* JavaScript buttons.
**/
global class EinsteinServices {
    /**
* This method calls the Einstein Sentiment
* API class to find the Sentiment of a block
* of text.
* @return SentimentAnalysisResponse Returns an instance of SentimentAnalysisResponse class
*
* @param text The text whose Sentiment has to be analysed
*/
    
    webservice Static SentimentAnalysisResponse findSentiment(String text) {
        EinsteinAPI api = new EinsteinAPI();
        SentimentAnalysisResponse resp = api.findSentiment( text );
        
        return resp;
    }
    
}