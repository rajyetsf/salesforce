public with sharing class AddAccountExtension {
    
    public Account accRec {get;set;}
    public List<Contact> contactList {get;set;}
    
    public List<Contact> contactSaveList {get;set;}
    //Test add
    public Id accId {get;set;}
    
    public AddAccountExtension(ApexPages.StandardController controller) {
        
        accRec = (Account)controller.getRecord();
        contactList = new List<Contact>();
        contactSaveList = new List<Contact>();
        
        /*for(Integer i = 1; i<=5 ; i++)
        {
            Contact contactObject = new Contact();
            contactList.add(contactObject);
        }*/
    }
    
    public PageReference save()
    {
        try{
            insert accRec;
            accId = accRec.Id;
        }
        Catch(Exception e)
        {
            System.debug('Exception = '+e);
        }
        
        if(accId != null)
        {
            for(Contact contRec : contactList)
            {
                if(contRec.FirstName != null)
                {
                    contRec.AccountId = accId;
                    contRec.LastName = contRec.FirstName;
                    
                    contactSaveList.add(contRec);
                }
            }
            
            if(contactSaveList.size() > 0)
            {
                try{
                    insert contactSaveList;
                }
                Catch(Exception e)
                {
                    System.debug('Exception = '+e);
                }
                
            }
            
            PageReference pageRef = new PageReference('/'+accId);
            return pageRef;
        }
        
        
        return null;
    }
    
    public PageReference newContact()
    {
        Contact contactObject = new Contact();
        contactList.add(contactObject);
        
        return null;
    }
    

}