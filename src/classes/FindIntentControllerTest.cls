@isTest
public class FindIntentControllerTest {
    
    public static void TestDataFactory(){
        
        Einstein_API_Settings__c settings = new Einstein_API_Settings__c();
        settings.Name='TestSetting';
        settings.Intent_Dataset_CSV__c='https://drive.google.com/file/d/1_6nJ9hFHhsbcfp6LTApJu5lD9k5nmVDM/view';
        settings.Intent_Dataset_Details_Endpoint__c = 'https://api.einstein.ai/v2/language/datasets/#DSID#';
        settings.Intent_Dataset_Training_Status_Endpoint__c='https://api.einstein.ai/v2/language/train/#MODELID#';
        settings.Intent_Train_Dataset_Endpoint__c='https://api.einstein.ai/v2/language/train';
        settings.Intent_Upload_Dataset_Endpoint__c='https://api.einstein.ai/v2/language/datasets/upload';
        settings.Predict_Intent_Endpoint__c='https://api.einstein.ai/v2/language/intent';
        settings.Registered_Email__c='rajyeturi16@gmail.com';
        settings.Sentiment_Endpoint__c='https://api.einstein.ai/v2/language/sentiment';
        settings.Sentiment_Model_Id__c='CommunitySentiment';
        settings.Token_Endpoint__c='https://api.einstein.ai/v2/oauth2/token';
        settings.Token_Expiration_Seconds__c=3600;
        insert settings;
        
        ContentVersion cv=new Contentversion();
        cv.title='einstein_platform';
        cv.PathOnClient ='test';
        Blob b=blob.valueOf('MIIEpAIBAAKCAQEAgFHmrdlUcd5FyXcBckjTMQHAC4OK/h3rVkIHMXJNB2yPe7FY'+
                            'zG4deldjKfIBdXPt82OrgnL1PuA4vQ6Ns6Oerj/eM4lSw97H4S4BHHG2I8VhFp3x'+
                            'M8Y5ATf/OabDHhNCMCCLzcT2BdRxEeFH/zluNfMhHmxxIKSYQue2hw/y+9g/B3+d'+
                            'llFZl9Pnwt2/Wp6PILVlWUaazpgF6PpHaJaV08sVzSRa0ZEwJnwBJZf0U5JdSdpB'+
                            'IVzbwhGxVc0P6meSBiKaPyhOU7hOg8DqQZ6EPdbvKsds2YCasYL2REn6CUg9O+rV'+
                            'tJDij352Sb2oMFRHxo/nWVPYF/V61xRb4Xg83QIDAQABAoIBABBiZMHt7gpcBy0i'+
                            'NN0Sz2SChLLzqNupllL9O03k+lDoW6m/i0cPCFSGs/K3I9iLZOae41dUtlvA5X2+'+
                            'TfmR+sDlBsotzpOT7pGh/iJTop4Opcbtg87IkJ7HN59025ULgHgEr9UmQQOdjJJr'+
                            '4XWFL71TvfbUgd1ZJtfkHjpAnxXo5yf734UxbqShzNPuolcp2JZAM5N3RsGxlQuG'+
                            '+2H8eN2Z+F25Vl4ZO/0vW36iK7lYqa2x86cIX3tCS8UFcc0ffTOXWfgPQqpGaSBz'+
                            'G9jCHmw52T3lcki6EGxzoqU2cUMvGmMt3cY/aFt/7hf4JJN/5AsF4ONQqDh8SkLA'+
                            'Wy2Ax0ECgYEA42vUYopMfbt6hljsquJyS13WV8YVQ7ja9rdZb6izJ16gvJb2WFTg'+
                            'Ezsc4E3oYtvYDGw7K4AAlFMTcpU7akLcU3VjMyVlkCoK4YITOLMpxm7uiUxybwsJ'+
                            'OssxI7nd38GtgONuFYvjX0RK1s8RZzJyFhATNyMukGZ5fcVS3EygjvsCgYEAkHH4'+
                            'Vuo3KIYvutBh9XRfqwoRK1I/tkKEErVrxGKlbK5ZKAcjHauxyIgmFSgIVmCWfbQm'+
                            'JAo7SMp44/lP2PjCPX4CDBMddW1wIuJ3eWRsM1XAlRyJXFUvoUqKJAOJRgnrFdny'+
                            'r+FMo0QYaGpUQKKz87Q5wQ/rKwYG6vzfa5P8vAcCgYEAhisveZMt3V+lwWzk80v1'+
                            'MhGCSvZJxVJhm/w/f2h/iVKCccB+RU6Ng8r0FkDgQLKCm5GetuMldFwutQLlsqMP'+
                            'aQ1wINSHPq4PDUu4G1NX6gG4KLg5KtnP8CfALduR0lhpDAkhAQUudx3LKT+wfLl+'+
                            'JRYGwBi5d+d89X5cjCuI7dECgYA6lB3UkbPA2Xpt1qiDdKh/otuHqBlyftM1CyIj'+
                            'SKYxxxsdIpfZJpI48w4Osmd/MDiXzgFRyFx2jrYsLiV1ppyPd5S49N+TichaPWGa'+
                            'd2FyCCrLzgDe0yU30hTnug9nugn+mJ7VORdtduY3Ijf85n74LHTnmN8UWaNglSC+'+
                            'bgum7QKBgQCA5CUGwPb/faGnYiX1ET4UzVHPN63IK4pedzJiOglfeQ1XYdBvDcjV'+
                            '1IOfoJG+0WfBg9dt2sPDNzqEWTjJf2QLIz0ORtRv2/8TGZc0GsqC/UI1b3iHgOoC'+
                            'N6jsdMWm2WwuJe3ZRJC2k5HK77pmUxfmZRx1DRVEnjFsdmHxJHIIog==');        
        cv.VersionData = b;
        system.debug('cv.VersionData'+cv.VersionData);
        insert cv;
    }
    
    public static testmethod void uploadDatasetTest(){
        
        TestDataFactory();
        Test.setMock(HttpCalloutMock.class, new FindIntentControllerMockGenerator());
        Test.startTest();
        
        DatasetUploadResponse Datasetupload =FindIntentController.uploadDataset();
        system.debug('DatasetUploadResponse ::'+Datasetupload);
        System.assert(Datasetupload != null);
        System.assert(Datasetupload.id != null);
        System.assert(Datasetupload.name != null);
        System.assertEquals(Datasetupload.name,'case_routing_intent.csv');
        System.assert(Datasetupload.createdAt != null);
        System.assert(Datasetupload.updatedAt != null);
        System.assert(Datasetupload.labelSummary != null);
        System.assert(Datasetupload.totalExamples != null);
        System.assert(Datasetupload.available != null);
        System.assert(Datasetupload.statusMsg != null);
        System.assert(Datasetupload.type_Z == null);
        System.assert(Datasetupload.object_Z == null);
        
        
        
        
        
        
        Test.stopTest();
        
    }
    
    public static testmethod void DatasetDetailsResponseTest(){
        
        TestDataFactory();
        Test.setMock(HttpCalloutMock.class, new FindIntentControllerMockGenerator());
        Test.startTest();
        
        DatasetDetailsResponse DataSetDetails = FindIntentController.getDatasetDetails('12345');
        system.debug('DatasetDetailsResponse :: '+DataSetDetails);
        System.assert(DataSetDetails != null);
        System.assert(DataSetDetails.id != null);
        System.assert(DataSetDetails.name != null);
        System.assert(DataSetDetails.createdAt != null);
        System.assert(DataSetDetails.updatedAt != null);
        System.assert(DataSetDetails.labelSummary != null);
        System.assert(DataSetDetails.totalExamples != null);
        System.assert(DataSetDetails.totalLabels != null);
        System.assert(DataSetDetails.available == true);
        System.assert(DataSetDetails.statusMsg != null);
        System.assert(DataSetDetails.type_Z == null);
        System.assert(DataSetDetails.object_Z == null);
        Test.stopTest();
        
    }
    
    public static testmethod void TrainDatasetResponseTest(){
        TestDataFactory();
        Test.setMock(HttpCalloutMock.class, new FindIntentControllerMockGenerator());
        Test.startTest();
        
        TrainDatasetResponse TrainDataSet = FindIntentController.trainDataset('12345');
        system.debug('TrainDataSetResponse :: '+TrainDataSet);
        System.assert(TrainDataSet != null);
        System.assert(TrainDataSet.createdAt != null);
        System.assert(TrainDataSet.datasetId != null);
        System.assert(TrainDataSet.datasetVersionId != null);
        System.assert(TrainDataSet.learningRate != null);
        System.assert(TrainDataSet.modelType != null);
        System.assert(TrainDataSet.epochs != null);
        System.assert(TrainDataSet.modelId != null);
        System.assert(TrainDataSet.name != null);
        System.assert(TrainDataSet.queuePosition != null);
        System.assert(TrainDataSet.queuePosition != null);
        System.assert(TrainDataSet.status != null);         
        System.assert(TrainDataSet.trainParams == null);
        System.assert(TrainDataSet.trainStats == null);
        System.assert(TrainDataSet.progress != null);
        System.assert(TrainDataSet.updatedAt != null);
        Test.stopTest();
    }
    public static testmethod void TrainStatusTest(){
        TestDataFactory();
        Test.setMock(HttpCalloutMock.class, new FindIntentControllerMockGenerator());
        Test.startTest();
        Object TrainStatus = FindIntentController.getDatasetTrainingStatus('678910');
        system.debug('TrainStatus'+TrainStatus);
        System.assert(TrainStatus !=null);
        Test.stopTest();
        
    }
    
    public static testmethod void PredictionResponseTest(){
        TestDataFactory();
        Test.setMock(HttpCalloutMock.class, new FindIntentControllerMockGenerator());
        Test.startTest();
        PredictionResponse predict = FindIntentController.predictIntent('12345', '67819');          
        predictionResponse.Probabilities probability = new PredictionResponse.Probabilities();
        probability.label = predict.Probabilities[0].label;
        probability.probability= predict.probabilities[0].probability;
        system.debug('PredictionResponse'+predict);
        System.assert(predict != null);
        System.assert(predict.Probabilities != null);
        System.assert(probability.label != null);
        System.assert( probability.probability != null);
        Test.stopTest();
    }
    
    public static testmethod void SentimentAnalysisResponseTest(){
        EinsteinAPI AI = new EinsteinAPI();
        TestDataFactory();
        Test.setMock(HttpCalloutMock.class, new FindIntentControllerMockGenerator());
        Test.startTest();
        
        //   FindIntentController.getDatasetTrainingStatus('modelForException');
        SentimentAnalysisResponse sentiment = AI.findSentiment('test');
        system.debug('SentimentAnalysisResponse::'+sentiment);
        SentimentAnalysisResponse.Probabilities prob = new SentimentAnalysisResponse.Probabilities();
        prob.label = sentiment.Probabilities[0].label;
        prob.probability= sentiment.probabilities[0].probability;
        system.debug('PredictionResponse'+sentiment);
        System.assert(sentiment != null);
        System.assert(sentiment.Probabilities != null);
        System.assertEquals(sentiment.Probabilities.size(), 3);
        System.assert(prob.label != null);
        System.assert( prob.probability != null);
        Test.stopTest();
    }
}