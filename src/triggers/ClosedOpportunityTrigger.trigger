trigger ClosedOpportunityTrigger on Opportunity (after insert, after update) {
   List <task> tasklist = new List <task>();
for(Opportunity opp : [SELECT Id,Name FROM Opportunity
                     WHERE Id IN :Trigger.New AND
                     StageName IN ('Closed Won','Closed Lost')]) {
        		tasklist.add(new task(subject='Follow Up Test Task',WhatId = opp.id));
    }
    if (tasklist.size() > 0) {
        insert tasklist;
    }
}